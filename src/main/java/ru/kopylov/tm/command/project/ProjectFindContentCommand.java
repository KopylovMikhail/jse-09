package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.util.CommandUtil;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class ProjectFindContentCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "project-find";
    }

    @Override
    public @NotNull String getDescription() {
        return "Shows all projects that contain the search word in the name or description.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS FIND]" +
                "\nENTER THE WORD:");
        @Nullable final String findWord = bootstrap.getTerminalService().getReadLine();
        @NotNull List<Project> projects = bootstrap.getProjectService().findByContent(findWord);
        CommandUtil.printProjectListWithParam(projects);
    }

}
