package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.util.CommandUtil;
import ru.kopylov.tm.util.ProjectComparator;
import ru.kopylov.tm.util.TaskComparator;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        @NotNull final List<Task> taskList = bootstrap.getTaskService().findAll(currentUserId);
        System.out.println("[SORTED BY CREATE]");
        CommandUtil.printTaskListWithParam(taskList);
        System.out.println("\nFOR SORT LIST TYPE ANY OF THESE COMMANDS:" +
                "\n     by-date-start" +
                "\n     by-date-finish" +
                "\n     by-state" +
                "\nFOR RETURN TO MAIN MENU PRESS [ENTER]\n");
        @Nullable final String sortCommand = bootstrap.getTerminalService().getReadLine();
        if ("by-date-start".equals(sortCommand)) {
            System.out.println("[SORTED BY DATE START]");
            taskList.sort(TaskComparator.byDateStart);
            CommandUtil.printTaskListWithParam(taskList);
        }
        if ("by-date-finish".equals(sortCommand)) {
            System.out.println("[SORTED BY DATE FINISH]");
            taskList.sort(TaskComparator.byDateFinish);
            CommandUtil.printTaskListWithParam(taskList);
        }
        if ("by-state".equals(sortCommand)) {
            System.out.println("[SORTED BY STATE]");
            taskList.sort(TaskComparator.byState);
            CommandUtil.printTaskListWithParam(taskList);
        }
    }

}
