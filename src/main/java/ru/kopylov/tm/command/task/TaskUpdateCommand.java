package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.util.CommandUtil;
import ru.kopylov.tm.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UPDATE]\n" +
                "ENTER EXISTING TASK NAME:");
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        @NotNull final List<Task> taskList = bootstrap.getTaskService().findAll(currentUserId);
        CommandUtil.printTaskList(taskList);
        final int taskNumber = Integer.parseInt(bootstrap.getTerminalService().getReadLine());
        @NotNull final Task task = taskList.get(taskNumber - 1);
        System.out.println("ENTER NEW TASK NAME OR PRESS [ENTER] TO SKIP:");
        @Nullable String terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty())
            task.setName(terminalText);
        System.out.println("ENTER TASK DESCRIPTION OR PRESS [ENTER] TO SKIP:");
        terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty())
            task.setDescription(terminalText);
        System.out.println("ENTER TASK DATE START (DD.MM.YYYY) OR PRESS [ENTER] TO SKIP:");
        terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(terminalText);
            task.setDateStart(dateStart);
        }
        System.out.println("ENTER TASK DATE FINISH (DD.MM.YYYY) OR PRESS [ENTER] TO SKIP:");
        terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(terminalText);
            task.setDateFinish(dateFinish);
        }
        int count = 1;
        System.out.println("ENTER TASK STATE NUMBER OR PRESS [ENTER] TO SKIP:");
        for (State state : State.values()) {
            System.out.println(count++ + ". " + state.getDisplayName());
        }
        terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty()) {
            final int stateNumber = Integer.parseInt(terminalText);
            task.setState(State.values()[stateNumber-1]);
        }
        final boolean updateSuccess = bootstrap.getTaskService().merge(task);
        if (updateSuccess) System.out.println("[TASK HAS BEEN UPDATED]\n");
        else System.out.println("SUCH A TASK DOES NOT EXIST OR NAME IS EMPTY.\n");
    }

}
